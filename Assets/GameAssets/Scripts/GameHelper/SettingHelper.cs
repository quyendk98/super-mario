﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameHelper
{
    public class SettingHelper : MonoBehaviour
    {
        [SerializeField] private List<GameObject> items = new List<GameObject>();
        [SerializeField] private Button mainButton;
        [SerializeField] private Vector3 offset;

        [SerializeField] private float rotationDuration;
        [SerializeField] private Ease rotationEase;

        [Header("Animation config")] [SerializeField]
        private float expandDuration;

        [SerializeField] private float collapseDuration;
        [SerializeField] private Ease expandEase;
        [SerializeField] private Ease collapseEase;

        [SerializeField] private float expandFadeDuration;
        [SerializeField] private float collapseFadeDuration;

        private Vector3 _spacing;
        private Transform _mainButtonT;
        private bool _isExpand;

        private void Start()
        {
            mainButton.onClick.AddListener(ShowMenu);
            _mainButtonT = mainButton.transform;
            _mainButtonT.SetAsLastSibling();

            _spacing = new Vector3(0, -Screen.height / 9f + 30f, 0f);
            ResetPosition();
        }

        private void ResetPosition()
        {
            foreach (var x in items)
            {
                x.transform.position = _mainButtonT.position;
            }
        }

        private void ShowMenu()
        {
            _isExpand = !_isExpand;

            if (_isExpand)
            {
                for (var i = 0; i < items.Count; i++)
                {
                    var pos = _mainButtonT.position + _spacing * (i + 1) - (i == 0 ? offset : Vector3.zero);
                    items[i].transform.DOMove(pos, expandDuration).SetEase(expandEase);
                    items[i].GetComponentInChildren<Image>().DOFade(1f, expandFadeDuration).From(0f);
                }
            }
            else
            {
                foreach (var t in items)
                {
                    t.transform.DOMove(_mainButtonT.position, collapseDuration).SetEase(collapseEase);
                    t.GetComponentInChildren<Image>().DOFade(0f, collapseFadeDuration);
                }
            }

            mainButton.transform.DORotate(Vector3.forward * 180f, rotationDuration).From(Vector3.zero)
                .SetEase(rotationEase);
        }

        private void OnDestroy()
        {
            mainButton.onClick.RemoveListener(ShowMenu);
        }
    }
}