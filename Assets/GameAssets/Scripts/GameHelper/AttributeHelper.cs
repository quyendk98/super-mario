#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;

namespace GameAssets.Scripts.GameHelper
{
    public static class AttributeHelper
    {
        public static object GetTargetObjectOfProperty(this SerializedProperty prop)
        {
            var path = prop.propertyPath.Replace(".Array.data[", "[");
            object obj = prop.serializedObject.targetObject;
            var elements = path.Split('.');

            foreach (var x in elements)
            {
                if (x.Contains("["))
                {
                    var elementName = x.Substring(0, x.IndexOf("[", StringComparison.Ordinal));
                    var index = Convert.ToInt32(x.Substring(x.IndexOf("[", StringComparison.Ordinal))
                        .Replace("[", "")
                        .Replace("]", ""));
                    obj = GetValue(obj, elementName, index);
                }
                else
                {
                    obj = GetValue(obj, x);
                }
            }
            
            return obj;
        }

        private static object GetValue(object source, string name)
        {
            if (source == null)
            {
                return null;
            }

            var type = source.GetType();

            while (type != null)
            {
                var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);

                if (f != null)
                {
                    return f.GetValue(source);
                }

                var p = type.GetProperty(name,
                    BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);

                if (p != null)
                {
                    return p.GetValue(source, null);
                }

                type = type.BaseType;
            }

            return null;
        }

        private static object GetValue(object source, string name, int index)
        {
            if (!(GetValue(source, name) is IEnumerable enumerable))
            {
                return null;
            }

            var ie = enumerable.GetEnumerator();

            for (var i = 0; i <= index; i++)
            {
                if (!ie.MoveNext())
                {
                    return null;
                }
            }

            return ie.Current;
        }

        public static TValue GetOrAdd<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key,
            Func<TValue> constructor)
        {
            if (dict.TryGetValue(key, out var value))
            {
                return value;
            }

            value = constructor();
            dict[key] = value;
            return value;
        }
    }
}
#endif