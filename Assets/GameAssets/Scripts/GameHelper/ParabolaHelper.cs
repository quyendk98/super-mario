using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameAssets.Scripts.GameHelper
{
    public class ParabolaHelper : MonoBehaviour
    {
        [SerializeField] private GameObject parabolaRoot;
        [SerializeField] private float speed = 1;

        [SerializeField] private bool autostart = true;
        [SerializeField] private new bool animation = true;

        private ParabolaFly _gizmos;
        private ParabolaFly _parabolaFly;

        private float _animationTime = float.MaxValue;
        private const int ACCURACY = 50;

        public bool NextParabola { get; private set; }

        private void OnDrawGizmos()
        {
            _gizmos ??= new ParabolaFly(parabolaRoot.transform);
            _gizmos.RefreshTransforms(1f);

            if ((_gizmos.points.Length - 1) % 2 != 0)
            {
                return;
            }

            var prevPos = _gizmos.points[0].position;

            for (var c = 1; c <= ACCURACY; c++)
            {
                var currTime = c * _gizmos.GetDuration() / ACCURACY;
                var currPos = _gizmos.GetPositionAtTime(currTime);
                var mag = (currPos - prevPos).magnitude * 2;

                Gizmos.color = new Color(mag, 0, 0, 1);
                Gizmos.DrawLine(prevPos, currPos);
                Gizmos.DrawSphere(currPos, 0.01f);
                prevPos = currPos;
            }
        }

        private void Start()
        {
            _parabolaFly = new ParabolaFly(parabolaRoot.transform);

            if (!autostart)
            {
                return;
            }

            RefreshTransforms(speed);
            FollowParabola();
        }

        private void Update()
        {
            NextParabola = false;

            switch (animation)
            {
                case true when _parabolaFly != null && _animationTime < _parabolaFly.GetDuration():
                {
                    _parabolaFly.GetParabolaIndexAtTime(_animationTime, out var parabolaIndexBefore);
                    _animationTime += Time.deltaTime;

                    _parabolaFly.GetParabolaIndexAtTime(_animationTime, out var parabolaIndexAfter);
                    transform.position = _parabolaFly.GetPositionAtTime(_animationTime);

                    if (parabolaIndexBefore != parabolaIndexAfter)
                    {
                        NextParabola = true;
                    }

                    break;
                }

                case true when _parabolaFly != null && _animationTime > _parabolaFly.GetDuration():
                    _animationTime = float.MaxValue;
                    animation = false;
                    break;
            }
        }

        public void FollowParabola()
        {
            RefreshTransforms(speed);
            _animationTime = 0f;
            transform.position = _parabolaFly.points[0].position;
            animation = true;
        }

        public Vector3 GetHighestPoint(int parabolaIndex)
        {
            return _parabolaFly.GetHighestPoint(parabolaIndex);
        }

        public Transform[] GetPoints => _parabolaFly.points;

        public Vector3 GetPositionAtTime(float time)
        {
            return _parabolaFly.GetPositionAtTime(time);
        }

        public float GetDuration => _parabolaFly.GetDuration();

        public void StopFollow()
        {
            _animationTime = float.MaxValue;
        }

        public void RefreshTransforms(float spd)
        {
            _parabolaFly.RefreshTransforms(spd);
        }

        public static float DistanceToLine(Ray ray, Vector3 point)
        {
            // see: http://answers.unity3d.com/questions/62644/distance-between-a-ray-and-a-point.html
            return Vector3.Cross(ray.direction, point - ray.origin).magnitude;
        }

        public static Vector3 ClosestPointInLine(Ray ray, Vector3 point)
        {
            return ray.origin + ray.direction * Vector3.Dot(ray.direction, point - ray.origin);
        }

        public class ParabolaFly
        {
            public readonly Transform[] points;
            private readonly Parabola3D[] _parabolas;
            private readonly float[] _partDuration;
            private float _completeDuration;

            public ParabolaFly(Component parabolaRoot)
            {
                var components =
                    new List<Component>(parabolaRoot.GetComponentsInChildren(typeof(Transform)));
                var transforms = components.ConvertAll(c => (Transform) c);

                transforms.Remove(parabolaRoot.transform);
                transforms.Sort((a, b) => string.Compare(a.name, b.name, StringComparison.Ordinal));
                points = transforms.ToArray();

                if ((points.Length - 1) % 2 != 0)
                {
                    throw new UnityException("ParabolaRoot needs odd number of points");
                }

                _parabolas = new Parabola3D[(points.Length - 1) / 2];
                _partDuration = new float[_parabolas.Length];
            }

            public Vector3 GetPositionAtTime(float time)
            {
                GetParabolaIndexAtTime(time, out var parabolaIndex, out var timeInParabola);
                var percent = timeInParabola / _partDuration[parabolaIndex];
                return _parabolas[parabolaIndex].GetPositionAtLength(percent * _parabolas[parabolaIndex].Length);
            }

            public void GetParabolaIndexAtTime(float time, out int parabolaIndex)
            {
                GetParabolaIndexAtTime(time, out parabolaIndex, out _);
            }

            public void GetParabolaIndexAtTime(float time, out int parabolaIndex, out float timeInParabola)
            {
                //f(x) = axÂ² + bx + c
                timeInParabola = time;
                parabolaIndex = 0;

                //determine parabola
                while (parabolaIndex < _parabolas.Length - 1 && _partDuration[parabolaIndex] < timeInParabola)
                {
                    timeInParabola -= _partDuration[parabolaIndex];
                    parabolaIndex++;
                }
            }

            public float GetDuration()
            {
                return _completeDuration;
            }

            public Vector3 GetHighestPoint(int parabolaIndex)
            {
                return _parabolas[parabolaIndex].GetHighestPoint();
            }

            public void RefreshTransforms(float speed)
            {
                if (speed <= 0f)
                {
                    speed = 1f;
                }

                if (points == null)
                {
                    return;
                }

                _completeDuration = 0;

                for (var i = 0; i < _parabolas.Length; i++)
                {
                    _parabolas[i] ??= new Parabola3D();
                    _parabolas[i].Set(points[i * 2].position, points[i * 2 + 1].position,
                        points[i * 2 + 2].position);
                    _partDuration[i] = _parabolas[i].Length / speed;
                    _completeDuration += _partDuration[i];
                }
            }
        }

        public class Parabola3D
        {
            public float Length { get; private set; }

            public Vector3 a;
            public Vector3 b;
            public Vector3 c;

            protected Parabola2D parabola2D;
            protected Vector3 h;

            protected bool tooClose;

            public Parabola3D()
            {
            }

            public Parabola3D(Vector3 a, Vector3 b, Vector3 c)
            {
                Set(a, b, c);
            }

            public void Set(Vector3 a1, Vector3 b1, Vector3 c1)
            {
                a = a1;
                b = b1;
                c = c1;
                RefreshCurve();
            }

            public Vector3 GetHighestPoint()
            {
                var d = (c.y - a.y) / parabola2D.Length;
                var e = a.y - c.y;

                var parabolaComplete =
                    new Parabola2D(parabola2D.A, parabola2D.B + d, parabola2D.C + e, parabola2D.Length);
                var e1 = new Vector3
                {
                    y = parabolaComplete.E.y,
                    x = a.x + (c.x - a.x) * (parabolaComplete.E.x / parabolaComplete.Length),
                    z = a.z + (c.z - a.z) * (parabolaComplete.E.x / parabolaComplete.Length)
                };

                return e1;
            }

            public Vector3 GetPositionAtLength(float length)
            {
                //f(x) = axÂ² + bx + c
                var percent = length / Length;
                var x = percent * (c - a).magnitude;

                if (tooClose)
                {
                    x = percent * 2f;
                }

                var pos = a * (1f - percent) + c * percent + h.normalized * parabola2D.F(x);

                if (tooClose)
                {
                    pos.Set(a.x, pos.y, a.z);
                }

                return pos;
            }

            private void RefreshCurve()
            {
                tooClose = Vector2.Distance(new Vector2(a.x, a.z), new Vector2(b.x, b.z)) < 0.1f &&
                           Vector2.Distance(new Vector2(b.x, b.z), new Vector2(c.x, c.z)) < 0.1f;
                Length = Vector3.Distance(a, b) + Vector3.Distance(b, c);

                if (!tooClose)
                {
                    RefreshCurveNormal();
                }
                else
                {
                    RefreshCurveClose();
                }
            }


            private void RefreshCurveNormal()
            {
                //                        .  E   .
                //                   .       |       point[1]
                //             .             |h         |       .
                //         .                 |       ___v1------point[2]
                //      .            ______--vl------    
                // point[0]---------
                //

                //lower v1
                var rl = new Ray(a, c - a);
                var v1 = ClosestPointInLine(rl, b);

                //get A=(x1,y1) B=(x2,y2) C=(x3,y3)
                Vector2 a2d, b2d, c2d;

                a2d.x = 0f;
                a2d.y = 0f;

                b2d.x = Vector3.Distance(a, v1);
                b2d.y = Vector3.Distance(b, v1);

                c2d.x = Vector3.Distance(a, c);
                c2d.y = 0f;

                parabola2D = new Parabola2D(a2d, b2d, c2d);

                //lower v
                //var p = parabola.E.x / parabola.Length;
                //Vector3 vl = points[0].position * (1f - p) + points[2].position * p;

                //h
                h = (b - v1) / Vector3.Distance(v1, b) * parabola2D.E.y;
            }

            private void RefreshCurveClose()
            {
                //distance to x0 - x2 line = |(x1-x0)x(x1-x2)|/|x2-x0|
                var fc01 = a.y <= b.y ? 1f : -1f;
                var fc02 = a.y <= c.y ? 1f : -1f;

                Vector2 a2d, b2d, c2d;
                //get A=(x1,y1) B=(x2,y2) C=(x3,y3)
                a2d.x = 0f;
                a2d.y = 0f;

                //b = sqrt(cÂ²-aÂ²)
                b2d.x = 1f;
                b2d.y = Vector3.Distance((a + c) / 2f, b) * fc01;

                c2d.x = 2f;
                c2d.y = Vector3.Distance(a, c) * fc02;

                parabola2D = new Parabola2D(a2d, b2d, c2d);
                h = Vector3.up;
            }
        }

        public class Parabola2D
        {
            public Vector2 E { get; private set; }

            public float A { get; }
            public float B { get; }
            public float C { get; }
            public float Length { get; }

            public Parabola2D(float a, float b, float c, float length)
            {
                A = a;
                B = b;
                C = c;

                SetMetadata();
                Length = length;
            }

            public Parabola2D(Vector2 a, Vector2 b, Vector2 c)
            {
                //f(x) = axÂ² + bx + c
                //a = (x1(y2 - y3) + x2(y3 - y1) + x3(y1 - y2)) / ((x1 - x2)(x1 - x3)(x3 - x2))
                //b = (x1Â²(y2 - y3) + x2Â²(y3 - y1) + x3Â²(y1 - y2))/ ((x1 - x2)(x1 - x3)(x2 - x3))
                //c = (x1Â²(x2y3 - x3y2) + x1(x3Â²y2 - x2Â²y3) + x2x3y1(x2 - x3))/ ((x1 - x2)(x1 - x3)(x2 - x3))
                var divisor = (a.x - b.x) * (a.x - c.x) * (c.x - b.x);

                if (divisor == 0f)
                {
                    a.x += 0.00001f;
                    b.x += 0.00002f;
                    c.x += 0.00003f;
                    divisor = ((a.x - b.x) * (a.x - c.x) * (c.x - b.x));
                }

                A = (a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y)) / divisor;
                B = (a.x * a.x * (b.y - c.y) + b.x * b.x * (c.y - a.y) + c.x * c.x * (a.y - b.y)) / divisor;
                C = (a.x * a.x * (b.x * c.y - c.x * b.y) + a.x * (c.x * c.x * b.y - b.x * b.x * c.y) +
                     b.x * c.x * a.y * (b.x - c.x)) / divisor;

                B *= -1f; //hack
                SetMetadata();
                Length = Vector2.Distance(a, c);
            }

            public float F(float x)
            {
                return A * x * x + B * x + C;
            }

            private void SetMetadata()
            {
                //derive
                //a*xÂ²+b*x+c = 0
                //2ax+b=0
                //x = -b/2a
                var x = -B / (2 * A);
                E = new Vector2(x, F(x));
            }
        }
    }
}