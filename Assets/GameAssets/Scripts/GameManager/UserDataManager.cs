﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameModel;
using GameAssets.Scripts.General;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameManager
{
    public class UserDataManager : BaseSingleton<UserDataManager>
    {
        public UserDataSave userDataSave;

        protected override void InitAwake()
        {
            base.InitAwake();

            if (GetDataFromSave() == null)
            {
                ResetUserData();
            }

            userDataSave = GetDataFromSave();
        }

        protected override void InnerUpdate()
        {
            base.InnerUpdate();

            if (Time.frameCount % Const.MIN_INTERVAL != 0)
            {
                return;
            }

            if (userDataSave.seconds >= 59)
            {
                userDataSave.minutes++;
                userDataSave.seconds = 0;
            }

            if (userDataSave.minutes >= 59)
            {
                userDataSave.hours++;
                userDataSave.minutes = 0;
            }

            userDataSave.seconds++;
        }

        private static UserDataSave GetDataFromSave()
        {
            var json = Decrypt(PlayerPrefs.GetString(Const.SAVE_DATA));

            if (json.Length > 0)
            {
                Debug.Log($"GAME DATA ===> {json}");
            }

            return JsonUtility.FromJson<UserDataSave>(json);
        }

        private static string Encrypt(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            var keyBytes =
                new Rfc2898DeriveBytes(Const.PASSWORD_HASH, Encoding.ASCII.GetBytes(Const.SALT_KEY)).GetBytes(256 / 8);

            var symmetricKey = new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.Zeros
            };

            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(Const.VI_KEY));
            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }

                memoryStream.Close();
            }

            return Convert.ToBase64String(cipherTextBytes);
        }

        private static string Decrypt(string encryptedText)
        {
            var cipherTextBytes = Convert.FromBase64String(encryptedText);
            var keyBytes =
                new Rfc2898DeriveBytes(Const.PASSWORD_HASH, Encoding.ASCII.GetBytes(Const.SALT_KEY)).GetBytes(256 / 8);

            var symmetricKey = new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.None
            };

            var decrypt = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(Const.VI_KEY));
            var memoryStream = new MemoryStream(cipherTextBytes);

            var cryptoStream = new CryptoStream(memoryStream, decrypt, CryptoStreamMode.Read);
            var plainTextBytes = new byte[cipherTextBytes.Length];
            var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }

        [Button]
        private void Save()
        {
            var json = JsonUtility.ToJson(userDataSave);
            Debug.Log($"GAME DATA SAVED ===> {json}");
            PlayerPrefs.SetString(Const.SAVE_DATA, Encrypt(json));
            PlayerPrefs.Save();
        }

        [Button]
        private void ClearUserData()
        {
            PlayerPrefs.DeleteAll();
        }

        [Button]
        private void ResetUserData()
        {
            userDataSave = new UserDataSave
            {
                level = 0,
                diamond = 0,
                money = 1000,
                death = 0,
                hours = 0f,
                minutes = 0f,
                seconds = 0f,
                sound = true,
                music = true,
                vibration = true,
                removedAds = false,
                highscores = new List<Highscore>(),
                abilities = new List<Ability>()
            };

            Save();
        }

        public void SetMusic(bool enable)
        {
            userDataSave.music = enable;
            Save();
        }

        public void SetSound(bool enable)
        {
            userDataSave.sound = enable;
            Save();
        }

        public void SetVibration(bool enable)
        {
            userDataSave.vibration = enable;
            Save();
        }

        public bool UnlockLevel(int level)
        {
            return userDataSave.level >= level;
        }

        public void LevelUp()
        {
            userDataSave.level++;
            Save();
        }

        public void SelectLevel(int level)
        {
            userDataSave.selectLevel = level;
            Save();
        }

        public void AddDeath()
        {
            userDataSave.death++;
            Save();
        }

        public bool CheckAndChangeMoney(float price)
        {
            if (userDataSave.money <= 0 || userDataSave.money < price)
            {
                return false;
            }

            AddMoney(-price);
            return true;
        }

        public void AddMoney(float value, bool saved = true)
        {
            userDataSave.money += value;

            if (saved)
            {
                Save();
            }
        }

        public void AddDiamond(float value, bool saved = true)
        {
            userDataSave.diamond += value;

            if (saved)
            {
                Save();
            }
        }

        public void AddHighscore(params Highscore[] hs)
        {
            userDataSave.highscores.AddRange(hs);
            Save();
        }

        public void AddAbilities(params Ability[] u)
        {
            userDataSave.abilities.AddRange(u);
            Save();
        }

        public void SetAbilities(params Ability[] u)
        {
            userDataSave.abilities.Clear();
            AddAbilities(u);
        }
        
        public void SetRemovedAds()
        {
            userDataSave.removedAds = true;
            Save();
        }
    }
}