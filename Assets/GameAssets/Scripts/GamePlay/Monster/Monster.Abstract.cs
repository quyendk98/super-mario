using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameInterface;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Monster
{
    public sealed partial class Monster : IStatus
    {
        public void Hurt()
        {
            Death();
        }

        public void HurtBullet()
        {
            Death();
        }

        public void Death()
        {
            this.IgnoreRaycast();
            UIManager.Instance.AddScore(transform, score);
            AudioManager.Instance.PlaySoundLoop("Sound Kill Enemy");
            
            rb.ResetInertia();
            ChangeAnimation(0);
            stat.ChangeCharacterState(CharacterState.Death);
            animator.OnComplete(() => gameObject.SetActive(false));
        }
    }
}