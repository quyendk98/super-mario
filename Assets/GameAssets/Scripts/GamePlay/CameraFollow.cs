﻿using DG.Tweening;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace GameAssets.Scripts.GamePlay
{
    public class CameraFollow : BaseSingleton<CameraFollow>
    {
        [SerializeField] private ParticleSystem confettiFx;

        [HideIf("@this.autoFindTarget || this.useTilemap")] [SerializeField]
        private Transform player;

        [ShowIf("@this.useTilemap && !this.autoFindTarget")] [SerializeField]
        private Tilemap tilemap;

        [Range(0f, 1f)] [SerializeField] private float lerpSpeed = 0.25f;
        [SerializeField] private bool useTilemap;
        [SerializeField] private bool autoFindTarget;

        private Vector3 _originOffset;
        private float _xMin;
        private float _xMax;

        private float _yMin;
        private float _yMax;

        private void Start()
        {
            if (useTilemap)
            {
                if (autoFindTarget)
                {
                    tilemap = FindObjectOfType<Tilemap>();
                }

                SetLimit();
            }
            else
            {
                if (player == null)
                {
                    player = Player.Player.Instance.transform;
                }
                
                if (player != null)
                {
                    _originOffset = transform.position - player.position;
                }
            }
        }

        protected override void InnerLateUpdate()
        {
            if (useTilemap)
            {
                if (tilemap == null)
                {
                    return;
                }

                var position = player.position;
                var target = new Vector3(Mathf.Clamp(position.x, _xMin, _xMax),
                    Mathf.Clamp(position.y, _yMin, _yMax), -10);
                transform.position = Vector3.Lerp(transform.position, target, lerpSpeed);
            }
            else
            {
                if (player == null)
                {
                    return;
                }

                transform.DOMove(player.position + _originOffset, lerpSpeed);
            }
        }

        private void SetLimit()
        {
            var min = tilemap.CellToWorld(tilemap.cellBounds.min);
            var max = tilemap.CellToWorld(tilemap.cellBounds.max);

            var height = 2f * GameManager.GameManager.Instance.Camera.orthographicSize;
            var width = height * GameManager.GameManager.Instance.Camera.aspect;

            _xMin = min.x + width / 1.5f;
            _xMax = max.x - width / 2f;

            _yMin = min.y + height / 1.5f;
            _yMax = max.y - height / 2f;
        }

        public void EnableConfetti()
        {
            if (confettiFx == null)
            {
                return;
            }

            confettiFx.SetActive(true);
        }
    }
}