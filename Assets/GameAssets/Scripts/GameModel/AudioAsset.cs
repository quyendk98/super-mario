﻿using GameAssets.Scripts.GameManager;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameModel
{
    [CreateAssetMenu(fileName = "Audio Asset", menuName = "UI/Audio Asset", order = 0)]
    public class AudioAsset : ScriptableObject
    {
        [Header("Sound")] [SerializeField] private Sprite uiSoundOn;
        [SerializeField] private Sprite uiSoundOff;

        [Header("Music")] [SerializeField] private Sprite uiMusicOn;
        [SerializeField] private Sprite uiMusicOff;

        [Header("Vibration")] [SerializeField] private Sprite uiVibrationOn;
        [SerializeField] private Sprite uiVibrationOff;

        public void SetMusic(Image imgMusic, bool change = false)
        {
            if (change)
            {
                UserDataManager.Instance.SetMusic(!UserDataManager.Instance.userDataSave.music);
            }

            if (imgMusic != null)
            {
                imgMusic.sprite = UserDataManager.Instance.userDataSave.music ? uiMusicOn : uiMusicOff;
            }
        }

        public void SetSound(Image imgSound, bool change = false)
        {
            if (change)
            {
                UserDataManager.Instance.SetSound(!UserDataManager.Instance.userDataSave.sound);
            }

            if (imgSound != null)
            {
                imgSound.sprite = UserDataManager.Instance.userDataSave.sound ? uiSoundOn : uiSoundOff;
            }
        }

        public void SetVibration(Image imgVibration, bool change = false)
        {
            if (change)
            {
                UserDataManager.Instance.SetVibration(!UserDataManager.Instance.userDataSave.vibration);
            }

            if (imgVibration != null)
            {
                imgVibration.sprite = UserDataManager.Instance.userDataSave.vibration ? uiVibrationOn : uiVibrationOff;
            }
        }
    }
}