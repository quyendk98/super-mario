﻿using GameAssets.Scripts.GameManager;
using UnityEngine;

namespace GameAssets.Scripts.GameModel
{
    [CreateAssetMenu(fileName = "Ability", menuName = "UI/Ability", order = 0)]
    public class Ability : ScriptableObject
    {
        [Header("Level")] public int baseLevel = 1;
        public int maxLevel = 5;

        [Header("Point")] public int basePoint = 1;
        public int addPoint = 1;

        [Header("Price")] public float basePrice = 1;
        public float addPrice = 1;

        public bool CanUpgrade()
        {
            if (baseLevel >= maxLevel || !UserDataManager.Instance.CheckAndChangeMoney(basePrice))
            {
                return false;
            }

            UserDataManager.Instance.AddMoney(-basePrice, false);
            baseLevel++;
            
            basePrice += addPrice;
            basePoint += addPoint;
            return true;
        }

        public void SetUpgrade(Ability u)
        {
            baseLevel = u.baseLevel;
            basePrice = u.basePrice;
            basePoint = u.basePoint;
        }
    }
}