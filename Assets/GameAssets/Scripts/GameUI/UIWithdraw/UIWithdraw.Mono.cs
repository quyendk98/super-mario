using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GameUI.UIWithdraw
{
    public partial class UIWithdraw
    {
        private void Awake()
        {
            btnBack.onClick.AddListener(() =>
            {
                Screen.orientation = ScreenOrientation.Landscape;
                UIManager.Instance.HideAllAndShowUI(UIType.UIPlay);
            });

            btnWithdraw.onClick.AddListener(() =>
            {
                if (_loading)
                {
                    ShowPopup("Withdrawing");
                    return;
                }
                
                if (string.IsNullOrEmpty(inputAddress.text))
                {
                    ShowPopup("You must enter address withdraw");
                    return;
                }
                
                if (string.IsNullOrEmpty(inputNetwork.text))
                {
                    ShowPopup("You must choose network withdraw");
                    return;
                }
                
                if (_coin > UserDataManager.Instance.userDataSave.money)
                {
                    ShowPopup("You don't enough coin to withdraw");
                    return;
                }

                StartCoroutine(Withdraw());
            });

            btnMax.onClick.AddListener(() =>
            {
                inputAmount.text = $"{UserDataManager.Instance.userDataSave.money}";
            });

            btnPopupOk.onClick.AddListener(() =>
            {
                inputAmount.text = string.Empty;
                popup.SetActive(false);
            });

            inputAmount.onValueChanged.AddListener(OnValueChanged);
        }

        private void OnDestroy()
        {
            inputAmount.onValueChanged.RemoveAllListeners();
        }

        private void OnEnable()
        {
            Screen.orientation = ScreenOrientation.Portrait;
            txtAvailable.text = $"Khả dụng: {UserDataManager.Instance.userDataSave.money:N1} Mario";
        }
    }
}