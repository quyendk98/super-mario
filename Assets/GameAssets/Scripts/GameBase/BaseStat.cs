using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GameBase
{
    public sealed class BaseStat : MonoBehaviour
    {
        public CharacterState characterState;
        public PriorityLevel priorityLevel;

        public float hp;
        public float atk;
        public float atkSpeed;
        public float atkRange;
        public float vision;
        public float moveSpeed;

        private bool IsMove { get; set; }
        public bool IsAttack { get; set; }
        public bool IsAlive { get; private set; } = true;

        public float Hp { get; set; }

        private void OnEnable()
        {
            ResetHp();
        }

        public float ResetHp()
        {
            Hp = hp;
            ChangeCharacterState(CharacterState.IdleOrRespawn);
            return Hp;
        }

        public float AddHp(float h)
        {
            Hp += h;
            return !(Hp < 0) ? Hp : 0f;
        }

        public void ChangeHp(float h)
        {
            hp += h;
            Hp = hp;
        }

        public bool IsCharacterState(CharacterState state)
        {
            return characterState.Equals(state);
        }

        public void SetCharacterState(CharacterState state)
        {
            characterState = state;
        }

        public bool ChangeCharacterState(CharacterState state)
        {
            if (IsCharacterState(state))
            {
                return false;
            }

            SetCharacterState(state);
            /*Debug.Log($"{name} CS ===> {state}");*/

            switch (state)
            {
                case CharacterState.IdleOrRespawn:
                    IsMove = false;
                    IsAlive = true;
                    IsAttack = false;
                    break;

                case CharacterState.Move:
                    IsMove = true;
                    IsAttack = false;
                    IsAlive = true;
                    break;

                case CharacterState.Attack:
                    IsMove = false;
                    IsAttack = true;
                    IsAlive = true;
                    break;

                case CharacterState.Death:
                    IsMove = false;
                    IsAttack = false;
                    IsAlive = false;
                    break;
            }

            return true;
        }
    }
}